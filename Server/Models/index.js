var mongoose = require('mongoose');

//CREATE A MODEL
var Complaint = mongoose.model('Complaints', {
  complaintID: {
    type: Number,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    minLength: 1,
    trim: true
  },
  contact: {
    type: Number,
    required: true,
  },
  email: {
    type: String,
    minLength: 1,
    trim: true
  },
  location: {
    longitude: {
      type: Number,
      required: true
    },
    lattitude: {
      type: Number,
      required: true
    }
  },
  address: {
    type: String,
    required: true,
    minLength: 1
  },
  complaintType: {
    type: String,
    required: true,
    minLength: 1
  },
  image: {
    type: String,
    required: true,
    minLength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    required: true,
    default: false
  },
  completedAt: {
    type: Date,
    default: null
  }
})

module.exports = {Complaint}
