//CONFIGURE MONGODB
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/Complaints');

module.exports = {mongoose};
