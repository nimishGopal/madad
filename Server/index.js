const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');

const {mongoose} = require('./db');
const {Complaint} = require('./Models');

var app = express();
const port = process.env.PORT || 3000;

//MiddleWare
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// FOR USER
app.post('/complaint', (req, res) => {
  var complaint = new Complaint(req.body);
  complaint.save().then((doc) => {
    res.send(doc);
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/complaint/:name', (req, res) => {
  var name = req.params.name;
  Complaint.find({name}).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

// FOR DEPT
app.get('/complaint', (req, res) => {
  Complaint.find().then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/resolved', (req, res) => {
  Complaint.find({completed: true}).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/complaint/:id', (req, res) => {
  var id = req.params.id;
  if(!ObjectID.isValid(id)){
    res.status(404).send('ID not found')
  }
  Complaint.findById(id).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.delete('/complaint/:id', (req, res) => {
  var id = req.params.id;
  if(!ObjectID.isValid(id)){
    res.status(404).send('ID not found')
  }
  Complaint.findByIdAndRemove(id).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.delete('/complaint', (req, res) => {
  Complaint.remove({}).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
});

//UPDATE
app.patch('/complaint/:id', (req, res) => {
  var id = req.params.id;
  var body = _.pick(req.body, ['completed', 'completedAt']);
  if(!ObjectID.isValid(id)){
    res.status(404).send('ID not found')
  }
  if(_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime()
  }
  else {
    body.completed = false;
    body.completedAt = null
  }

  Complaint.findByIdAndUpdate(id, {$set: body}, {new: true}).then((complaint) => {
    res.send({complaint});
  }, (e) => {
    res.status(400).send(e);
  });
})


app.listen(port, () => {
  console.log(`Started on port ${port}`);
});
